var productsResponse;
var customerResponse;

function getProducts(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    /*
      Ready States
      1 - Prepare
      2 - Send
      3 - Wait Answer
      4 - Answered
    */
    if(this.readyState == 4 && this.status == 200){
      productsResponse = request.responseText;
      //console.table(JSON.parse(productsResponse).value);
      processProducts();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function processProducts(){
  var jsonProducts = JSON.parse(productsResponse);
  //alert(jsonProducts.value[0].ProductName);
  var divTable = document.getElementById("divTable");
  var table = document.createElement("table");
  var tbody = document.createElement("tbody");
  table.classList.add("table");
  table.classList.add("table-striped");
  for (var i = 0; i < jsonProducts.value.length; i++) {
    var newRow = document.createElement("tr");
    var columnName = document.createElement("td");
    columnName.innerText = jsonProducts.value[i].ProductName;
    var columnPrice = document.createElement("td");
    columnPrice.innerText = jsonProducts.value[i].UnitPrice;
    var columnStock = document.createElement("td");
    columnStock.innerText = jsonProducts.value[i].UnitsInStock;
    newRow.appendChild(columnName);
    newRow.appendChild(columnPrice);
    newRow.appendChild(columnStock);
    tbody.appendChild(newRow);
  }
  table.appendChild(tbody);
  divTable.appendChild(table);
}

function getCustomers(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    /*
      Ready States
      1 - Prepare
      2 - Send
      3 - Wait Answer
      4 - Answered
    */
    if(this.readyState == 4 && this.status == 200){
      customerResponse = request.responseText;
      //console.table(JSON.parse(customerResponse).value);
      processCustomers();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function processCustomers(){
  var jsonCustomer = JSON.parse(customerResponse);
  //alert(jsonProducts.value[0].ProductName);
  var divTable = document.getElementById("divTableCustomer");
  var table = document.createElement("table");
  var tbody = document.createElement("tbody");
  var urlImg = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  table.classList.add("table");
  table.classList.add("table-striped");
  for (var i = 0; i < jsonCustomer.value.length; i++) {
    var newRow = document.createElement("tr");
    var columnName = document.createElement("td");
    columnName.innerText = jsonCustomer.value[i].CompanyName;
    var columnAddress = document.createElement("td");
    columnAddress.innerText = jsonCustomer.value[i].ContactName;
    var columnPostalCode = document.createElement("td");
    columnPostalCode.innerText = jsonCustomer.value[i].City;
    var columnFlag = document.createElement("td");
    var imgFlag = document.createElement("img");
    var country = jsonCustomer.value[i].Country;
    if (country == 'UK') {
      country = 'United-Kingdom';
    }
    imgFlag.src = urlImg + country + ".png";
    imgFlag.classList.add("flag");

    columnFlag.appendChild(imgFlag);
    newRow.appendChild(columnName);
    newRow.appendChild(columnAddress);
    newRow.appendChild(columnPostalCode);
    newRow.appendChild(columnFlag);

    tbody.appendChild(newRow);
  }
  table.appendChild(tbody);
  divTable.appendChild(table);
}
